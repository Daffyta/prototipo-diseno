import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-form',
  templateUrl: './form.component.html',
  styleUrls: ['./form.component.css']
})

export class FormComponent implements OnInit {
  escuelas: Array<any>;

  nombre: string;
  numeroAlumnos: string;
  director: string;

  message: string;

  escuela: any;

  constructor() {
  }

  ngOnInit() {
  	this.escuelas = new Array();
  	this.escuelas.push({nombre: "Test", alumnos: "10", director: "Juan"});
  }

  onAdd() : void {
  	if (this.nombre == "" || this.numeroAlumnos == "" || this.director == "")
  		this.message = "Error en los datos";
  	else {
  		this.message = "";
  		this.escuelas.push({nombre: this.nombre, alumnos: this.numeroAlumnos, director: this.director});
  		this.nombre = "";
  		this.numeroAlumnos = "";
  		this.director = "";
  	}
  }

  onSelect() : void {
  	this.message = "Escuela seleccionada " + this.escuela;
  }

}
