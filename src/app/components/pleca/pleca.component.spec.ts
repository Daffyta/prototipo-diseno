import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { PlecaComponent } from './pleca.component';

describe('PlecaComponent', () => {
  let component: PlecaComponent;
  let fixture: ComponentFixture<PlecaComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ PlecaComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PlecaComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
